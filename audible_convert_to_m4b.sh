#!/bin/bash

code=30d86304

while (( "$#" ))
do

        cd "${1%/*}" # gehe ins Verzeichnis

        FILENAME=${1##*/} # Dateiname ist alles ab dem letzten '/'
        echo "$FILENAME"
        # guck dir die Ausgabe erstmal an - wenn alles passt kannst Du das "echo" weglassen
        ffmpeg -activation_bytes $code -i "$FILENAME" -c:a copy -vn "${FILENAME%.*}.m4b" &
        shift
        cd -
done

#convert.sh code <Ordner>/*.ogg
